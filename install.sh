#!/bin/bash
#
# Script de instalação da ferramenta xtool
#
set -e

# Definição do diretório home da ferramenta
XTOOL_HOME="$HOME/.xtool"

#
# Verificação de dependências
#
echo -en "Procurando pela ferramenta yq..."
if [[ ! $(command -v yq) > /dev/null ]] ; then
  echo "[FAIL]"
  echo "Ferramenta não encontrada."
  echo "======================================================================================================"
  echo " Por favor, instale a ferramenta yq com os procedimentos abaixo: "
  echo " https://github.com/mikefarah/yq#install"
  echo ""
  echo "======================================================================================================"
  echo ""
  exit 1
fi
echo "[OK]"

echo -en "Procurando pelo java..."
if [[ ! $(command -v java) > /dev/null ]] ; then
  echo "[FAIL]"
  echo "Java não encontrado."
  echo "======================================================================================================"
  echo " Por favor, instale o java 8 com a ferramenta SDK: "
  echo " https://sdkman.io/install"
  echo ""
  echo "======================================================================================================"
  echo ""
  exit 1
fi
echo "[OK]"




